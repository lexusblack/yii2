<?php

namespace app\models;

use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
class Student extends ActiveRecord
{
    public function rules(): array
    {
        return [
            [['name', 'second_name', 'school_id'], 'required']
        ];
    }

    public function getSchool(): ActiveQueryInterface
    {
        return $this->hasOne(School::class, ['id' => 'school_id']);
    }
}