<?php

namespace app\models;

use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;

class School extends ActiveRecord
{
    public function rules(): array
    {
        return [
            [['school'], 'required']
        ];
    }

    public function getStudents(): ActiveQueryInterface
    {
        return $this->hasMany(Student::class, ['school_id' => 'id']);
    }
}