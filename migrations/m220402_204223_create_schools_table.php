<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%schools}}`.
 */
class m220402_204223_create_schools_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%schools}}', [
            'id' => $this->primaryKey(),
            'school' => $this->string()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%schools}}');
    }
}
