<?php
/**
 * @var \app\models\Student $model
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\School;
?>

<?php ActiveForm::$autoIdPrefix = 'a'; ?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'second_name') ?>

<?= $form->field($model, 'school_id')->dropDownList(
    ArrayHelper::map(School::find()->all(), 'id', 'school')
) ?>

<div class="form-group">
    <?= Html::submitButton(empty($model->id) ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>

</div>

<?php ActiveForm::end(); ?>



