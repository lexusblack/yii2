<?php
/**
 * @var array $student
 * @var string $pagination
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use lav45\widget\AjaxCreate;
?>
    <h1>Students</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Second name</th>
            <th>Class</th>
            <th>Control</th>
        </tr>
        </thead>
    <tbody>
        <?php foreach ($student as $item): ?>
            <tr>
                <td><?= Html::encode("{$item->id}") ?></td>
                <td><?= Html::encode("{$item->name}") ?></td>
                <td><?= Html::encode("{$item->second_name}") ?></td>
                <td><?= Html::encode("{$item->school['school']}") ?></td>
                <td><?= Html::a('Change', ['edit', 'id' => $item->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $item->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this student?',
                        'method' => 'post',
                    ],
                ]) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

<?= LinkPager::widget(['pagination' => $pagination]) ?>

<?php
AjaxCreate::begin();

echo Html::button('Create student', [
    'data-href' => Url::toRoute(['add']),
    'class' => 'btn btn-success',
]);

AjaxCreate::end();
?>