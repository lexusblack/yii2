<?php
/**
 * @var \app\models\School $model
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php ActiveForm::$autoIdPrefix = 'a'; ?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'school') ?>
    <div class="form-group">
        <?= Html::submitButton(empty($model->id) ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
