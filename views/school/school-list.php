<?php
/**
 * @var array $schools
 * @var string $pagination
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use lav45\widget\AjaxCreate;

?>
    <h1>Classes</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name of class</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($schools as $school): ?>
            <tr>
                <td><?= Html::encode("{$school->id}") ?></td>
                <td><?= Html::encode("{$school->school}") ?></td>
                <td>
                    <?= Html::a('Change', ['edit', 'id' => $school->id], ['class' => 'btn btn-primary']) ?>
                    <?php
                    AjaxCreate::begin();

                    echo Html::button('Delete', [
                        'data-href' => Url::toRoute(['delete', 'id' => $school->id]),
                        'class' => 'btn btn-danger',
                    ]);

                    AjaxCreate::end();
                    ?>
                    <?= Html::a('Select', ['student/sort', 'id' => $school->id], ['class' => 'btn btn-primary']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


<?= LinkPager::widget(['pagination' => $pagination]) ?>

<?php
AjaxCreate::begin();

echo Html::button('Create class', [
    'data-href' => Url::toRoute(['add']),
    'class' => 'btn btn-success',
]);

AjaxCreate::end();
?>


