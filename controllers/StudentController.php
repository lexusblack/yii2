<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Student;
use yii\data\Pagination;
use Yii;

class StudentController extends Controller
{
    protected array $homeUrl = ['index'];

    public function actionIndex(): string
    {
        $query = Student::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $student = $query
            ->orderBy('name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('student-list', [
            'student' => $student,
            'pagination' => $pagination
        ]);
    }

    public function actionAdd(): string|\yii\web\Response
    {
        $model = new Student();

        if (
            $model->load(Yii::$app->request->post()) &&
            $model->validate() &&
            $model->save()
        ) {
            return $this->redirect($this->homeUrl);
        }

        return $this->renderAjax('student-input', [
            'model' => $model,
        ]);
    }

    public function actionEdit(string $id): string|\yii\web\Response
    {
        $model = Student::find()->where(['id' => $id])->one();

        if (
            $model->load(Yii::$app->request->post()) &&
            $model->validate() &&
            $model->save()
        ) {
            return $this->redirect($this->homeUrl);
        }

        return $this->render('student-input', ['model' => $model]);
    }

    public function actionDelete(string $id): \yii\web\Response
    {
        $student = Student::findOne($id);
        $student->delete();

        return $this->redirect($this->homeUrl);
    }

    public function actionSort($id): string
    {

        $query = Student::find()
            ->where(['school_id' => $id]);

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $student = $query
            ->orderBy('name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('student-list', [
            'student' => $student,
            'pagination' => $pagination
        ]);

    }
}