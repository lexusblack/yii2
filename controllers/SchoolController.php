<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\School;
use app\models\Student;

class SchoolController extends Controller
{
    private array $homeUrl = ['index'];

    public function actionIndex(): string
    {
        $query = School::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count()
        ]);

        $schools = $query->orderBy('school')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('school-list', [
            'schools' => $schools,
            'pagination' => $pagination
        ]);
    }

    public function actionAdd(): string|\yii\web\Response
    {
        $model = new School();

        if (
            $model->load(Yii::$app->request->post()) &&
            $model->validate() &&
            $model->save()
        ) {
            return $this->redirect($this->homeUrl);
        }

        return $this->renderAjax('school-input', ['model' => $model]);
    }

    public function actionEdit(string $id): string|\yii\web\Response
    {
        $model = School::find()->where(['id' => $id])->one();
        if (
            $model->load(Yii::$app->request->post()) &&
            $model->validate() &&
            $model->save()
        ) {
            return $this->redirect($this->homeUrl);
        }

        return $this->render('school-input', ['model' => $model]);
    }

    public function actionDelete(string $id): string|\yii\web\Response
    {

        $school = School::findOne($id);

        if ($school->getStudents()->count() > 0) {
            return 'ERROR - First you need delete student from class!';
        }
        $school->delete();

        return $this->redirect($this->homeUrl);
    }

}